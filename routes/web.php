<?php


Route::get('/', function(){
  // authenticated user home page view
  if(Auth::check()){
    return redirect()->route('dashboard');
  }
  //guest user home page
  return view('index');
});

Route::get('/test', function(){
  return Auth::user()->test();
});
// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::middleware('auth')->group(function(){

  // Create a post
  Route::post('/post/create', 'PostsController@createPost')->name('post.create');

  //unfriend user
  Route::get('/unfriend/{id}', 'FriendsController@unfriendUser')->name('friend.unfriend');

  // view all friends 
  Route::get('/profile/{slug}/friends', 'ProfileController@getFriendsList')->name('profile.friends');

  // cancel friend request
  Route::get('/cancel_request/{id}', 'FriendsController@cancelFriendRequest')->name('friend.cancel');

  // Accepting friend requests
  Route::get('/accept_request/{id}', 'FriendsController@acceptFriendRequest')->name('friend.accept');

  // Getting user friend requests
  Route::get('/friend_requests', 'FriendsController@MyFriendRequests')->name('friend.myrequests');

  // sending a friend request
  Route::get('/send_request/{id}', 'FriendsController@sendRequest')->name('friend.request');

  // list all users for friend request view
  Route::get('/find_friends', 'FriendsController@allUsers')->name('friends.index');


  // user profile about update
  Route::put('/profile/about/{id}/update', 'ProfileController@updateProfile')->name('profile.update');

  // user profile about update view
  Route::get('/profile/{slug}/about/update', 'ProfileController@editProfile')->name('profile.about.update');

  // user profile about create 
  Route::post('/profile/about/edit', 'ProfileController@createAboutProfile')->name('profile.create');

  // user profile about edit view
  Route::get('/profile/{slug}/about/edit', 'ProfileController@editAboutProfile')->name('profile.about.edit');

  // user profile about view
  Route::get('/profile/{slug}/about', 'ProfileController@getAboutProfile')->name('profile.about');

  // upload profile picture
  Route::post('/profile/{id}/upload-pic', 'ProfileController@uploadPhoto')->name('upload.photo');

  //user profile route
  Route::get('/profile/{slug}', 'ProfileController@getIndex')->name('profile.index');

  // Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/feed', 'Homecontroller@index')->name('dashboard');
});
