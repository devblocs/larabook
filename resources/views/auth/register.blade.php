@extends('layouts.master')

@section('title', 'Register')

@section('content')
<div class="container">
  <div class="card-panel">
    <div class="row">
      <h2 class="center-align">Register</h2>
    </div>

    <div class="row">
      <div class="col s2">

      </div>
      <form class="col s8" action="{{ route('register') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
          <div class="input-field col s12 {{ $errors->has('name') ? ' has-error' : '' }}">
            <input id="name" type="text" name="name" class="validate">
            <label for="name" >Name</label>
              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="row">
          <div class="input-field col s12 {{ $errors->has('gender') ? ' has-error' : '' }}">
            <select name="gender">
              <option value="" disabled selected>Select Gender</option>
              <option value="m">Male</option>
              <option value="f">Female</option>
            </select>
              @if ($errors->has('gender'))
                  <span class="help-block">
                      <strong>{{ $errors->first('gender') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="row">
          <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
            <label for="email">Email</label>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="row">
          <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" name="password" class="validate">
            <label for="password">Password</label>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="password-confirm" type="password" name="password_confirmation" class="validate">
            <label for="password-confirm">Confirm Password</label>
          </div>
        </div>
        <div class="center-align">
          <button class="btn waves-effect waves-light blue darken-2" type="submit" name="submit">Register</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
