@extends('layouts.master')

@php
  $fname = $user->first_name;
  $lname = $user->last_name;

  $fullname = ucwords($fname . " " . $lname);
@endphp

@section('title',  $fullname . ' | About')

@section('content')

<div class="container">

        <!-- Timeline
        ================================================= -->
        <div class="timeline">
                @include('partials._profile_nav')
          <div id="page-contents">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-7">

                <!-- About
                ================================================= -->
                <div class="about-profile">
                  @if(!$user->profile)
                    <h4>No about section</h4>
                  @else
                  <div class="about-content-block">
                    <h4 class="grey"><i class="ion-ios-information-outline icon-in-title"></i> About Me</h4>
                    <p>{{ ucfirst($user->profile->user_about) }}</p>
                  </div>

                  <div class="about-content-block">
                    <h4 class="grey"><i class="ion-ios-location-outline icon-in-title"></i>Location</h4>
                    <p>{{ ucfirst($user->profile->user_city) . ", " . ucfirst($user->profile->user_country) }}</p>
                  </div>

                  <div class="about-content-block">
                    <a href="{{ route('profile.about.update', ['slug' => $user->slug]) }}" class="btn btn-primary btn-large">Update Profile</a>
                  </div>
                @endif
                </div>
              </div>
              <div class="col-md-2 static">
                <div id="sticky-sidebar">
                  <h4 class="grey">Sarah's activity</h4>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> Commended on a Photo</p>
                      <p class="text-muted">5 mins ago</p>
                    </div>
                  </div>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> Has posted a photo</p>
                      <p class="text-muted">an hour ago</p>
                    </div>
                  </div>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> Liked her friend's post</p>
                      <p class="text-muted">4 hours ago</p>
                    </div>
                  </div>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> has shared an album</p>
                      <p class="text-muted">a day ago</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@endsection
