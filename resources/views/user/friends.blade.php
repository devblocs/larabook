@extends('layouts.master')

@php
  $fname = $user->first_name;
  $lname = $user->last_name;

  $fullname = ucwords($fname . " " . $lname);
@endphp

@section('title',  $fullname . ' | About')

@section('content')

<div class="container">

        <!-- Timeline
        ================================================= -->
        <div class="timeline">
                @include('partials._profile_nav')
          <div id="page-contents">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-7">

                    <!-- Friend List
                    ================================================= -->
                    <div class="friend-list">
                      <div class="row">
                        @foreach($friends as $friend)
                        <div class="col-md-6 col-sm-6">
                            <div class="friend-card">
                                <img src="http://placehold.it/1030x360" alt="profile-cover" class="img-responsive cover" />
                                <div class="card-info">
                                <img src="{{ asset('users/images/' . $friend->profile_pic) }}" alt="{{ $friend->first_name . ' ' . $friend->last_name }}" class="profile-photo-lg" />
                                <div class="friend-info">
                                    <a href="{{ route('friend.unfriend', [$user->id, 'friend_list']) }}" class="pull-right text-red">Unfriend</a>
                                    <h5><a href="{{ route('profile.index', $friend->slug) }}" class="profile-link">{{ ucfirst($friend->first_name . ' ' . $friend->last_name) }}</a></h5>

                                    @php
                                        if($friend->gender == 'm'){
                                            $gender = "Male";
                                        }else{
                                            $gender = "Female";
                                        }
                                    @endphp

                                    @if($friend->profile && $friend->profile->user_intro)
                                        <p>{{ ucfirst($friend->profile->user_intro ) }}</p>
                                    @else
                                        <p>{{ $gender }}</p>
                                    @endif
                                    
                                </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
              <div class="col-md-2 static">
                <div id="sticky-sidebar">
                  <h4 class="grey">Sarah's activity</h4>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> Commended on a Photo</p>
                      <p class="text-muted">5 mins ago</p>
                    </div>
                  </div>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> Has posted a photo</p>
                      <p class="text-muted">an hour ago</p>
                    </div>
                  </div>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> Liked her friend's post</p>
                      <p class="text-muted">4 hours ago</p>
                    </div>
                  </div>
                  <div class="feed-item">
                    <div class="live-activity">
                      <p><a href="#" class="profile-link">Sarah</a> has shared an album</p>
                      <p class="text-muted">a day ago</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@endsection
