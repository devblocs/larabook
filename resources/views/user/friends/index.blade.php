@extends('layouts.master')

@php
  $fname = Auth::user()->first_name;
  $lname = Auth::user()->last_name;

  $fullname = ucwords($fname . " " . $lname);
@endphp

@section('title', 'Find Friends')

@section('content')
    @if(Session::has('request_error'))
        <p class="bg-danger">{{ session('request_error') }}</p>
    @endif
  <div id="page-contents">
    	<div class="container">
    		<div class="row">

          <!-- Newsfeed Common Side Bar Left
          ================================================= -->
    			@include('partials.newsfeed._left_sidebar')

    			<div class="col-md-7">

                    <!-- Nearby People List
                    ================================================= -->
                    @foreach($users as $user)
                        @if(!Auth::user()->isFriendWith($user))
                    <div class="people-nearby">
                        <div class="nearby-user">
                            <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <img src="{{ asset('users/images/' . $user->profile_pic) }}" alt="{{ $user->first_name }}" class="profile-photo-lg" />
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <h5><a href="{{ route('profile.index', $user->slug) }}" class="profile-link">{{ ucfirst($user->first_name . " " . $user->last_name)}}</a></h5>
                                @if($user->profile['user_intro'] && $user->profile['user_country'])
                                    <p>{{ ucfirst($user->profile['user_intro']) }}</p>
                                    <p class="text-muted">{{ ucfirst($user->profile['user_city']) . ", " . ucfirst($user->profile['user_country']) }}</p>
                                @else
                                    @if($user->gender == 'm')
                                        <p>{{ 'Male' }}</p>
                                    @else
                                    <p>{{ 'Female' }}</p>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-3 col-sm-3">
                                @if(!Auth::user()->hasFriendRequestFrom($user))
                                    @if(Auth::user()->hasSentFriendRequestTo($user))
                                        <a href="#" class="btn btn-primary pull-right">Request Sent</a>
                                    @else
                                        <a href="{{ route('friend.request', $user->id) }}" class="btn btn-primary pull-right">Add Friend</a>
                                    @endif
                                @else
                                    <a href="{{ route('profile.index', $user->slug) }}" class="btn btn-primary pull-right">View Profile</a>         
                                @endif
                            </div>
                            </div>
                            @if(Auth::user()->hasFriendRequestFrom($user))
                            <div class="row">
                                    <hr />
                                    <div class="col-md-6">
                                            <a href="{{ route('friend.accept', $user->id) }}" class="btn btn-success">Accept Request</a> 
                                    </div>
                                    <div class="col-md-6">
                                            <a href="{{ route('friend.cancel', $user->id) }}" class="btn btn-danger">Cancel Request</a> 
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>

                

          <!-- Newsfeed Common Side Bar Right
          ================================================= -->
    			@include('partials.newsfeed._right_sidebar')
    		</div>
    	</div>
    </div>
@endsection
