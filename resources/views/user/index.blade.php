@extends('layouts.master')

@php
  $fname = Auth::user()->first_name;
  $lname = Auth::user()->last_name;

  $fullname = ucwords($fname . " " . $lname);
@endphp

@section('title', 'Welcome ' . $fullname)

@section('content')
  <div id="page-contents">
    	<div class="container">
    		<div class="row">

          <!-- Newsfeed Common Side Bar Left
          ================================================= -->
    			@include('partials.newsfeed._left_sidebar')

    			<div class="col-md-7">

            <!-- Post Create Box
            ================================================= -->
            <div class="create-post">
            	<div class="row">
                  {!! Form::open(['route' => 'post.create', 'method' => 'POST']) !!}
            		<div class="col-md-7 col-sm-7">
                  <div class="form-group">
                    <img src="{{ asset('users/images/' . Auth::user()->profile_pic) }}" alt="{{ $fullname }}" class="profile-photo-md" />
                    {!! Form::textarea('create_post', null, ['size' => '30x1', 'class' => 'form-control', 'id' => 'create_post', 'placeholder' => "What's your idea!"]) !!}
                  </div>
                </div>
            		<div class="col-md-5 col-sm-5">
                  <div class="tools">
                      {!! Form::submit('Publish', ['class' => 'btn btn-primary']) !!}
                  </div>
                </div>
                {!! Form::close() !!}
            	</div>
            </div><!-- Post Create Box End-->

            <!-- Post Content
            ================================================= -->
            @foreach($posts as $post)
            @if(Auth::user()->isFriendWith($post->user) || $post->user_id == Auth::user()->id)
            <div class="post-content">
              <div class="post-container">
                <img src="http://placehold.it/300x300" alt="user" class="profile-photo-md pull-left" />
                <div class="post-detail">
                  <div class="user-info">
                  <h5><a href="timeline.html" class="profile-link">{{ ucfirst($post->user->first_name . " " . $post->user->last_name) }}</a>
                    <p class="text-muted">Published a photo about {{ $post->created_at->diffForHumans() }}</p>
                  </div>
                  <div class="reaction">
                    <a class="btn text-green"><i class="icon ion-thumbsup"></i> 13</a>
                    <a class="btn text-red"><i class="fa fa-thumbs-down"></i> 0</a>
                  </div>
                  <div class="line-divider"></div>
                  <div class="post-text">
                    <p>{{ ucfirst($post->post_body) }}<i class="em em-anguished"></i> <i class="em em-anguished"></i> <i class="em em-anguished"></i></p>
                  </div>
                  <div class="line-divider"></div>
                  {{--  <div class="post-comment">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm" />
                    <p><a href="timeline.html" class="profile-link">Diana </a><i class="em em-laughing"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>
                  </div>
                  <div class="post-comment">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm" />
                    <p><a href="timeline.html" class="profile-link">John</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>
                  </div>
                  <div class="post-comment">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm" />
                    <input type="text" class="form-control" placeholder="Post a comment">
                  </div>  --}}
                </div>
              </div>
            </div>
              @endif
            @endforeach

          <!-- Newsfeed Common Side Bar Right
          ================================================= -->
    			{{--  @include('partials.newsfeed._right_sidebar')  --}}
    		</div>
    	</div>
    </div>
@endsection
