@extends('layouts.master')

@php
  $fname = $user->first_name;
  $lname = $user->last_name;

  $fullname = ucwords($fname . " " . $lname);
@endphp

@section('title',  $fullname . ' | Edit')

@section('content')

<div class="container">

        <!-- Timeline
        ================================================= -->
        <div class="timeline">
                @include('partials._profile_nav')
                <div id="page-contents">
                        <div class="row">
                          <div class="col-md-3">

                            <!--Edit Profile Menu-->
                            <ul class="edit-menu">
                                <li class="active"><i class="icon ion-ios-information-outline"></i><a href="edit-profile-basic.html">Basic Information</a></li>
                                {{--  <li><i class="icon ion-ios-briefcase-outline"></i><a href="edit-profile-work-edu.html">Education and Work</a></li>
                                <li><i class="icon ion-ios-heart-outline"></i><a href="edit-profile-interests.html">My Interests</a></li>
                              <li><i class="icon ion-ios-settings"></i><a href="edit-profile-settings.html">Account Settings</a></li>
                                <li><i class="icon ion-ios-locked-outline"></i><a href="edit-profile-password.html">Change Password</a></li>  --}}
                            </ul>
                          </div>
                          <div class="col-md-7">

                            <!-- Basic Information
                            ================================================= -->
                            <div class="edit-profile-container">
                              <div class="block-title">
                                <h4 class="grey"><i class="icon ion-android-checkmark-circle"></i>Edit basic information</h4>
                                <div class="line"></div>
                              </div>
                              <div class="edit-block">
                                <form name="basic-info" id="basic-info" class="form-inline" action="{{ route('profile.create') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="my-info">About me</label>
                                            <textarea id="my-info" name="about" class="form-control" placeholder="Some texts about you" rows="4" cols="400"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="user_intro">Intro</label>
                                            <input type="text" name="intro" class="form-control input-group-lg" id="user_intro" title="Enter your intro" placeholder="Enter Your Introduction" />
                                        </div>
                                    </div>
                                  <div class="row">
                                    <div class="form-group col-xs-6">
                                      <label for="city"> My city</label>
                                      <input id="city" class="form-control input-group-lg" type="text" name="city" title="Enter city" placeholder="Your city" />
                                    </div>
                                    <div class="form-group col-xs-6">
                                      <label for="country">My country</label>
                                      <select class="form-control" id="country" name="country">
                                        <option value="india">India</option>
                                      </select>
                                    </div>
                                  </div>
                                  <button class="btn btn-primary" type="submit" name="submit">Save Changes</button>
                                </form>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 static">

                            <!--Sticky Timeline Activity Sidebar-->
                            <div id="sticky-sidebar">
                              <h4 class="grey">Sarah's activity</h4>
                              <div class="feed-item">
                                <div class="live-activity">
                                  <p><a href="#" class="profile-link">Sarah</a> Commended on a Photo</p>
                                  <p class="text-muted">5 mins ago</p>
                                </div>
                              </div>
                              <div class="feed-item">
                                <div class="live-activity">
                                  <p><a href="#" class="profile-link">Sarah</a> Has posted a photo</p>
                                  <p class="text-muted">an hour ago</p>
                                </div>
                              </div>
                              <div class="feed-item">
                                <div class="live-activity">
                                  <p><a href="#" class="profile-link">Sarah</a> Liked her friend's post</p>
                                  <p class="text-muted">4 hours ago</p>
                                </div>
                              </div>
                              <div class="feed-item">
                                <div class="live-activity">
                                  <p><a href="#" class="profile-link">Sarah</a> has shared an album</p>
                                  <p class="text-muted">a day ago</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
        </div>
      </div>

@endsection
