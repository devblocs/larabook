@extends('layouts.master')

@section('title', "Login or Sign Up")

@section('content')
  <!-- Landing Page Contents
   ================================================= -->
   <div id="lp-register">
     <div class="container wrapper">
       <div class="row">
         <div class="col-sm-5">
           <div class="intro-texts">
             <h1 class="text-white">Make Cool Friends !!!</h1>
             <p>Friend Finder is a social network template that can be used to connect people. The template offers Landing pages, News Feed, Image/Video Feed, Chat Box, Timeline and lot more. <br /> <br />Why are you waiting for? Buy it now.</p>
             <button class="btn btn-primary">Learn More</button>
           </div>
         </div>
         <div class="col-sm-6 col-sm-offset-1">
           <div class="reg-form-container">

             <!-- Register/Login Tabs-->
             <div class="reg-options">
               <ul class="nav nav-tabs">
                 <li class="active"><a href="#register" data-toggle="tab">Register</a></li>
                 <li><a href="#login" data-toggle="tab">Login</a></li>
               </ul><!--Tabs End-->
             </div>

             <!--Registration Form Contents-->
             <div class="tab-content">
               <div class="tab-pane active" id="register">
                 <h3>Register Now !!!</h3>
                 <p class="text-muted">Be cool and join today. Meet millions</p>

                 <!--Register Form-->
                 <form name="registration_form" id='registration_form' method="POST" class="form-inline" action="{{ route('register') }}">

                    {{ csrf_field() }}
                   <div class="row">
                     <div class="form-group col-xs-6 {{ $errors->has('first_name') ? ' has-error' : '' }}">
                       <label for="firstname" class="sr-only">First Name</label>
                       <input id="firstname" class="form-control input-group-lg" type="text" name="first_name" title="Enter first name" placeholder="First name" value= "{{ old('first_name') }}"  required autofocus />

                       @if ($errors->has('first_name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('first_name') }}</strong>
                          </span>
                      @endif
                     </div>
                     <div class="form-group col-xs-6 {{ $errors->has('last_name') ? ' has-error' : '' }}">
                       <label for="lastname" class="sr-only">Last Name</label>
                       <input id="lastname" class="form-control input-group-lg" type="text" name="last_name" title="Enter last name" placeholder="Last name" value="{{ old('lastname') }}" required autofocus />

                       @if ($errors->has('last_name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('last_name') }}</strong>
                          </span>
                      @endif
                     </div>
                   </div>
                   <div class="row">
                     <div class="form-group col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                       <label for="email" class="sr-only">Email</label>
                       <input id="email" class="form-control input-group-lg" type="text" name="email" title="Enter Email" placeholder="Your Email" value="{{ old('email') }}" required />

                       @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                     </div>
                   </div>
                   <div class="row">
                     <div class="form-group col-xs-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                       <label for="password" class="sr-only">Password</label>
                       <input id="password" class="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Password" required />

                       @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                     </div>
                  </div>
                   <div class="row">
                     <div class="form-group col-xs-12">
                       <label for="confirmpassword" class="sr-only">Confirm Password</label>
                       <input id="confirmpassword" class="form-control input-group-lg" type="password" name="password_confirmation" title="Enter password" placeholder="Re-enter Password"/>
                     </div>
                   </div>
                   <div class="form-group gender">
                     <label class="radio-inline">
                       <input type="radio" name="gender" value="m" checked>Male
                     </label>
                     <label class="radio-inline">
                       <input type="radio" name="gender" value="f">Female
                     </label>
                   </div>
                   <div class="row">
                      <div class="form-group">
                       <div class="col-md-12">
                             <button type="submit" class="btn btn-primary">
                                  Register Now
                             </button>
                          </div>
                     </div>
                   </div>
                 </form><!--Register Now Form Ends-->
                 <p><a href="#">Already have an account?</a></p>
                 {{-- <button class="btn btn-primary">Register Now</button> --}}
               </div><!--Registration Form Contents Ends-->

               <!--Login-->
               <div class="tab-pane" id="login">
                 <h3>Login</h3>
                 <p class="text-muted">Log into your account</p>

                 <!--Login Form-->
                 <form name="Login_form" id='Login_form' method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    
                    <div class="row">
                     <div class="form-group col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                       <label for="my-email" class="sr-only">Email</label>
                       <input id="my-email" class="form-control input-group-lg" type="text" name="email" title="Enter Email" placeholder="Your Email" value="{{ old('email') }}" required autofocus />

                       @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                     </div>
                   </div>
                   <div class="row">
                     <div class="form-group col-xs-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                       <label for="my-password" class="sr-only">Password</label>
                       <input id="my-password" class="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Password" required/>
                     </div>
                   </div>
                   <div class="row">
                      <div class="form-group">
                          <div class="col-md-12">
                             <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                  </label>
                             </div>
                          </div>
                      </div>
                   </div>
                   <div class="row">
                     <div class="form-group col-xs-12">
                       <button class="btn btn-primary">Login Now</button>
                     </div>
                   </div>
                 </form><!--Login Form Ends-->
                 <p><a href="#">Forgot Password?</a></p>
               </div>
             </div>
           </div>
         </div>
       </div>
       <div class="row">
         <div class="col-sm-6 col-sm-offset-6">

           <!--Social Icons-->
           <ul class="list-inline social-icons">
             <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
             <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
             <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
             <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
             <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
           </ul>
         </div>
       </div>
     </div>
   </div>
@endsection
