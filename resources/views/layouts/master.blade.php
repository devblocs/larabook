{{-- *****************************
# Master Layout
****************************** --}}

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  @include('partials._head')
</head>
<body>

  @include('partials._navbar')

  @yield('content')

  @auth
    @include('partials._footer')
  @endauth

  @include('partials._preloader')
  @include('partials._scripts')

  </body>
</html>
