{{-- ***********************************
# Navigation for profile view
************************************ --}}

<!-- Timeline
================================================= -->
<div class="timeline">
  <div class="timeline-cover">

    <!--Timeline Menu for Large Screens-->
    <div class="timeline-nav-bar hidden-sm hidden-xs">
      <div class="row">
        <div class="col-md-3">
          <div class="profile-info">
              @if($user->id == Auth::user()->id)
            <div class="image-container">
              <img src="{{ asset('users/images/' . Auth::user()->profile_pic) }}" alt="{{ $fullname }}" class="img-responsive profile-photo" />
              <a href="#" data-toggle="modal" data-target="#myModal">
                <div class="overlay"></div>
                <span>Change Profile Picture</span>
              </a>
            </div>
            @else
            <img src="{{ asset('users/images/' . $user->profile_pic) }}" alt="{{ $fullname }}" class="img-responsive profile-photo" />
            @endif
            <h3>{{ $fullname }}</h3>
            @if($user->profile)
              <p class="text-muted">{{ ucfirst($user->profile->user_intro) }}</p>
            @endif
          </div>
        </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change Profile Picture</h4>
              </div>
              <div class="modal-body">
                <div class="row text-center">
                  <div class="col-md-12">
                        <img src="{{ asset('users/images/' . Auth::user()->profile_pic) }}" alt="{{ $fullname }}" class="img-circle modal-image" />

                  </div>
                </div>
                <div class="more-space">
                  {{-- more spacing --}}
                </div>
                <div class="row center-block">
                  <div class="col-md-12">
                    {{ Form::open(['method' => 'POST', 'route' => ['upload.photo', $user->id], 'files' => true]) }}
                      <div class="form-group pull-right">
                        {{ Form::file('profile_pic', ['class' => 'input-lg modal-file-input']) }}
                      </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  {{ Form::submit('upload photo', ['class' => 'btn btn-primary']) }}
              </div>
              {{ Form::close() }}
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <ul class="list-inline profile-menu">
            <li><a href="{{ route('profile.index', ['slug' => $user->slug]) }}" class="active">Timeline</a></li>
            <li><a href="{{ route('profile.about', ['slug' => $user->slug]) }}">About</a></li>
            <li><a href="timeline-album.html">Album</a></li>
            <li><a href="{{ route('profile.friends', ['slug' => $user->slug]) }}">Friends</a></li>
          </ul>
          <ul class="follow-me list-inline">
            {{-- <li>1,299 people following her</li> --}}
            @if($user->id == Auth::user()->id)
              @if (!$user->profile)
                <li><a href="{{ route('profile.about.edit', ['slug' => Auth::user()->slug]) }}" class="btn btn-primary">Add Profile</a></li>
              @else
                <li><a href="{{ route('profile.about.update', ['slug' => $user->slug]) }}" class="btn btn-primary">Update Profile</a></li>
              @endif

            @else
              @if(Auth::user()->hasSentFriendRequestTo($user))
                <li><a href="#" class="btn btn-primary pull-right">Request Sent</a></li>
              @elseif(Auth::user()->isFriendWith($user))
                <li><a href="{{ route('friend.unfriend', [$user->id, 'profile_nav']) }}" class="btn btn-danger">Unfriend</a></li>
              @elseif(Auth::user()->hasFriendRequestFrom($user))
                <li><a href="{{ route('friend.accept', $user->id) }}" class="btn btn-success">Accept Request</a></li>
                <li><a href="{{ route('friend.request', $user->id) }}" class="btn btn-danger">Cancel Request</a> </li>
              @else
                <li><a href="{{ route('friend.request', $user->id) }}" class="btn btn-primary pull-right">Add Friend</a></li>
              @endif
            @endif

          </ul>
        </div>
      </div>
    </div><!--Timeline Menu for Large Screens End-->

    <!--Timeline Menu for Small Screens-->
    <div class="navbar-mobile hidden-lg hidden-md">
      <div class="profile-info">
        <img src="{{ asset('users/images/' . Auth::user()->profile_pic) }}" alt="{{ $fullname }}" class="img-responsive profile-photo" />
        <h4>{{ $fullname }}</h4>
        <p class="text-muted">Creative Director</p>
      </div>
      <div class="mobile-menu">
        <ul class="list-inline">
          <li><a href="{{ route('profile.index', ['slug' => $user->slug]) }}" class="active">Timeline</a></li>
          <li><a href="{{ route('profile.about', ['slug' => $user->slug]) }}">About</a></li>
          <li><a href="timeline-album.html">Album</a></li>
          <li><a href="timeline-friends.html">Friends</a></li>
        </ul>
        <button class="btn-primary">Add Friend</button>
      </div>
    </div><!--Timeline Menu for Small Screens End-->
  </div>
