{{-- ***********************************
# External JS scripts for the master layout
************************************* --}}

<!-- Scripts
    ================================================= -->
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    {{ HTML::script('js/jquery.appear.min.js') }}
    {{ HTML::script('js/jquery.incremental-counter.js') }}
    @auth
      {{ HTML::script('js/jquery.sticky-kit.min.js') }}
      {{ HTML::script('js/jquery.scrollbar.min.js') }}
    @endauth
      @yield('external_scripts')
    {{ HTML::script('js/script.js') }}

@yield('scripts')
