{{-- ****************************************
# head tag elements and content of the master layout
******************************************** --}}


    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="robots" content="index, follow" />
		<title>Friend Finder | @yield('title')</title>

    <!-- Stylesheets
    ================================================= -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		{{ Html::style('css/style.css') }}
		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    {{-- <link rel="stylesheet" href="css/font-awesome.min.css" /> --}}

		<script src="https://use.fontawesome.com/e6728bcd93.js"></script>

    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">

    {{-- <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/> --}}

@yield('styles')
