{{-- **************************************
# Navigation bar for the master layout
***************************************** --}}

<!-- Header
    ================================================= -->
        @guest
            <header id="header-inverse">
        @else
            <header id="header">
        @endguest

      <nav class="navbar navbar-default navbar-fixed-top menu">
        <div class="container">

          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ asset('images/logo.png') }}" alt="logo" /></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              @guest
                  <form class="navbar-form navbar-right hidden-sm">
                    <div class="form-group">
                      <i class="icon ion-android-search"></i>
                      <input type="text" class="form-control" placeholder="Search friends, photos, videos">
                    </div>
                  </form>
              @else
                  <ul class="nav navbar-nav navbar-right main-menu">
                      <li>
                        <a href="{{ route('friends.index') }}">Find Friends</a>
                      </li>
                      <li>
                        <a href="{{ route('profile.index', ['slug' => Auth::user()->slug]) }}"><span><img src="{{ asset('users/images/' . Auth::user()->profile_pic) }}" alt="user" class="profile-photo-xs" />&nbsp;</span> {{ ucfirst(Auth::user()->first_name) }}</a>
                      </li>
                      <li>
                        <a href="{{ route('dashboard') }}">Home</a>
                      </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true" style="font-size: 20px;"></i></a>
                      <ul class="dropdown-menu newsfeed-home">
                      <li><a href="{{ route('friend.myrequests') }}">{!! Auth::user()->getFriendRequests()->count() == 0 ? "" : '<span class="label label-info">' . Auth::user()->getFriendRequests()->count() . '</span>' !!} Friend Requests </a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                      </ul>
                    </li>
                  </ul>
                  <form class="navbar-form navbar-right hidden-sm">
                    <div class="form-group">
                      <i class="icon ion-android-search"></i>
                      <input type="text" class="form-control" placeholder="Search friends, photos, videos">
                    </div>
                  </form>
              @endguest
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
      </nav>
    </header>
    <!--Header End-->
