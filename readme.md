# Larabook
A social network for connecting professionals.

## Project Description
Larabook is a social network for professionals of same interests. A place where people share their ideas, get suggestions and find funding for their products.

## Tech Specifications
- **Front-end:**
    - HTML5
    - CSS3
    - JavaScript
    - jQuery
    - Bootstrap

- **Back-end:**
    - Laravel(PHP)
    - MySQL

### Future Updates:
    1. Upgrading the project 
    2. Multi-auth system 
    3. Company pages
    4. groups
    5. Analytics for posts for general users and company posts
    
