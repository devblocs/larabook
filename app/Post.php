<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['post_body'];

    //post has 1 user
    public function user(){
        return $this->belongsTo('App\User');
    }
}
