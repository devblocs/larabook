<?php

namespace App;

use Hootlex\Friendships\Traits\Friendable;  # friendships trait
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Friendable; #use friendable trait in user model

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'gender', 'slug', 'profile_pic'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // an user profile relationship
    public function profile(){
      return $this->hasOne('App\UserProfile');
    }

    // a user profile relationship
    public function posts(){
        return $this->hasMany('App\Post');
    }
}
