<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    

    public function createPost(Request $request){
        $request->validate([
            'create_post' => 'required|max:200',
        ]);
        
    
        $create_post = Auth::user()->posts()->create([
            'post_body' => $request->input('create_post'),
        ]);

        if($create_post){
            return redirect()->back();
        }
    }
}
