<?php

namespace App\Http\Controllers;

use App\UserProfile;    #import user profile model
use App\User;   #import user model
use Illuminate\Support\Facades\Auth;  # auth facades
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * user profile controller
    */

    /**
     *  user profile timeline view
     */
    public function getIndex($slug){
    //   $user = Auth::user();

        $user = User::where('slug', $slug)->firstOrfail();
        return view('user.timeline', ['user' => $user]);
    }

    /**
     * authenticated user profile pic upload
     */
    public function uploadPhoto(Request $request, $id){

        $user = User::findOrFail($id);

        $path = "users/images";

        if($file = $request->file('profile_pic')){
            $name = time() . $file->getClientOriginalName();

            $file->move($path, $name);

            $user::where('id', $id)->update(['profile_pic' => $name]);

            return redirect()->route('profile.index', $user->slug);
        }else{
            return redirect()->back();
        }
    }

     /**
     * users profile about section view
     */
     public function getAboutProfile($slug){
        $user = User::where('slug', $slug)->firstOrfail();
      return view('user.about', ['user' => $user]);
    }

     /**
     * authenticated user first edit profile view
     */
    public function editAboutProfile($slug){
        $user = User::where('slug', $slug)->firstOrfail();
        if(Auth::user()->id == $user->id){
            return view('user.editBasicProfile', ['user' => $user]);
        }else{
            return redirect()->back();
        }
    }

     /**
     * Authenticated user create basic about profile logic
     */
    public function createAboutProfile(Request $request){
        $request->validate([
            'about' => 'required',
            'city'  => 'required|max:120',
            'country'   => 'required',
            'intro' => 'required|max:120',
        ]);

        $user = Auth::user();

        $user->profile()->create([
          'user_about' => $request->about,
          'user_city' => $request->city,
          'user_country' => $request->country,
          'user_intro' => $request->intro
        ]);

        return redirect()->route('profile.about', Auth::user()->slug);

    }

     /**
     * Authenticated user update basic about profile view
     */
    public function editProfile($slug){
        $user = User::where('slug', $slug)->firstOrfail();
        if(Auth::user()->id == $user->id){
            return view('user.updateBasicProfile', ['user' => $user]);
        }else{
            return redirect()->back();
        }
    }

     /**
     * Authenticated user update basic about profile logic 
     */
    public function updateProfile(Request $request, $id){

            $request->validate([
                'about' => 'required',
                'city'  => 'required|max:120',
                'country'   => 'required',
                'intro' => 'required|max:120',
            ]);

             $profile = UserProfile::findOrFail($id);

             $profile->update([
                 'user_about' => $request->about,
                 'user_city' => $request->city,
                 'user_country' => $request->country,
                 'user_intro' => $request->intro
             ]);

             return redirect()->route('profile.about', Auth::user()->slug);
    }

    public function getFriendsList($slug){
        $user = User::where('slug', $slug)->firstOrFail();

        $friends = $user->getFriends();

        return view('user.friends', ['user' => $user, 'friends' => $friends]);
    }
}
