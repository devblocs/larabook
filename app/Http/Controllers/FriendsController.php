<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\FriendRequest;
use Illuminate\Support\Facades\Auth;  # auth facades
use App\User;   #import user model
use Illuminate\Http\Request;

/**
 * TODO:
 *  - cancel friend request sent
 *  - get mutual friends
 */
class FriendsController extends Controller
{ 
    
    public function allUsers(){

        $users = User::where('id', '!=' , Auth::user()->id)->get();

        return view('user.friends.index', ['users' => $users]);
    }

    // send friend request
    public function sendRequest($id){
        $user = User::findOrFail($id);

        $status = Auth::user()->befriend($user);

        if($status){
            return redirect()->back();
        }else{
            Session::flash('request_error', 'Error in sending friend request');
        }
    }

    // authenticated user friend requests
    public function myFriendRequests(){
        $requests = Auth::user()->getFriendRequests();

        return view('user.friends.requests', ['requests' => $requests]);
    }

    // accepting the friend request
    public function acceptFriendRequest($id){
        $user = User::findOrFail($id);

        $status = Auth::user()->acceptFriendRequest($user);

        if($status){
            return redirect()->back();
        }
    }

    //cancel friend request
    public function cancelFriendRequest($id){
        $user = User::findOrFail($id);

        $status = Auth::user()->denyFriendRequest($user);

        if($status){
            return redirect()->back();
        }
    }

    // FIXME: route not working for friends list page
    public function unfriendUser($id, $link){
        $user = User::findOrFail($id);

        $status = Auth::user()->unfriend($user);

        if($status){
            if($link == 'friend_list'){
                return redirect()->route('profile.friends', $user->slug);
            }else{
                return redirect()->back();
            }
            
        }
    }

}
