<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = "user_profiles";

    //mass assignment
    protected $fillable = [
      'user_about', 'user_country', 'user_city', 'user_intro'
    ];

    // a user relation
    public function user(){
      return $this->belongsTo('App\User');
    }
}
